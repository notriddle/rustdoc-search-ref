rustdoc search syntax reference impl
====================================

## How to use it as a fuzz tester

First, build it with release mode, and keep track of the path:

    $ cargo build --release
    $ RUSTDOCSEARCHREF=`realpath target/release/rustdoc-search-ref`
    $ cd ../rust

Then, apply [rust.diff](rust.diff) to the source tree, to add an option for running it under a fuzz tester,
and execute the test suite.

    $ patch -p1 < ../rustdoc-search-ref/rust.diff
    $ ./x.py doc
    $ node src/tools/rustdoc-js/tester.js \
        --doc-folder build/x86_64-unknown-linux-gnu/doc/ \
        --crate-name std \
        --fuzz $RUSTDOCSEARCHREF \
        --resource-suffix 1.61.0

## How to just use it

    $ cargo run 'Test<Query>'
