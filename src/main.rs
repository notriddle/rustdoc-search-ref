use combine::error::ParseError;
use combine::parser::char::char;
use combine::stream::Stream;
use combine::{
    attempt, between, choice, from_str, many1, one_of, optional, satisfy, sep_by, sep_by1,
    skip_many, unexpected_any, value, EasyParser, Parser, eof,
};
use serde::{Deserialize, Serialize};
use serde_repr::*;

use std::str::FromStr;

// `impl Parser` can be used to create reusable parsers with zero overhead
fn query_<Input>() -> impl Parser<Input, Output = Query>
where
    Input: Stream<Token = char>,
    // Necessary due to rust-lang/rust#24159
    Input::Error: ParseError<Input::Token, Input::Range, Input::Position>,
{
    let skip_spaces = || skip_many(one_of("\t ".chars())).silent();
    let lex_char = |c| char(c).skip(skip_spaces());
    fn is_alpha(c: char) -> bool {
        match c as u32 {
            0x41..=0x5a | 0x61..=0x7a => true,
            _ => false,
        }
    }
    fn is_digit(c: char) -> bool {
        match c as u32 {
            0x30..=0x39 => true,
            _ => false,
        }
    }
    let type_sep = || one_of(",\t ".chars());
    fn is_ident_char(c: char) -> bool {
        is_alpha(c) || is_digit(c) || c == '_' || c == '!'
    }
    let ident = || {
        many1(satisfy(is_ident_char).expected("<ident char>"))
            .then(|v: Vec<char>| {
                let bang_count = v.iter().filter(|&&c| c == '!').count();
                if bang_count > 1 {
                    return unexpected_any("multiple !").right();
                }
                if bang_count == 1 && v.last() != Some(&'!') {
                    return unexpected_any("! must come at end").right();
                }
                value(String::from_iter(v)).left()
            })
    };
    let double_colon = || (char(':'), char(':'));
    let return_arrow = || (char('-'), char('>'));
    let path = || sep_by1::<Vec<String>, _, _, _>(ident(), double_colon());
    let arg_without_generics = path;
    let optional_arg_list_without_generics = || {
        sep_by::<Vec<Option<Vec<String>>>, _, _, _>(optional(arg_without_generics()), type_sep())
            .map(|v| {
                v.into_iter()
                    .filter_map(|ov| ov)
                    .collect::<Vec<Vec<String>>>()
            })
    };

    // Terminating a generics list is optional.
    let generics = || {
        between(
            char('<'),
            choice!(eof::<Input>().map(|()| '>'), char('>')),
            optional_arg_list_without_generics(),
        )
    };

    let arg = || (path(), optional(generics()));
    let arg_list = || {
        sep_by::<Vec<Option<(Vec<String>, Option<Vec<Vec<String>>>)>>, _, _, _>(
            optional(arg()),
            type_sep(),
        )
        .then(|v| {
            let v = v
                .into_iter()
                .filter_map(|ov| ov)
                .collect::<Vec<(Vec<String>, Option<Vec<Vec<String>>>)>>();
            value(v)
        })
    };
    let nonempty_arg_list = || {
        sep_by1::<Vec<Option<(Vec<String>, Option<Vec<Vec<String>>>)>>, _, _, _>(
            optional(arg()),
            type_sep(),
        )
        .then(|v| {
            let v = v
                .into_iter()
                .filter_map(|ov| ov)
                .collect::<Vec<(Vec<String>, Option<Vec<Vec<String>>>)>>();
            if v.is_empty() {
                unexpected_any("empty list").right()
            } else {
                value(v).left()
            }
        })
    };

    let return_args =
        || (return_arrow().skip(skip_spaces()), nonempty_arg_list()).map(|(_, args)| args);

    fn is_type_filter_char(c: char) -> bool {
        is_alpha(c) || is_digit(c)
    }
    let type_filter = || {
        from_str::<_, TypeFilter, _>(
            many1(satisfy(is_type_filter_char).expected("<type filter char>"))
                .map(|v: Vec<char>| String::from_iter(v)),
        )
    };

    let exact_search = || {
        (
            optional(attempt((type_filter().skip(skip_spaces()), lex_char(':'))).map(|(ty, _)| ty)),
            optional(return_arrow().skip(skip_spaces()).map(|_| ())),
            between(char('"'), char('"'), ident()),
            optional(generics()),
        )
    };

    // NOTE: rustdoc search.js grammar isn't really LL(1), so we need to cheat here.
    enum TypeFilterOrFirstComponentOrFirstElementOrError {
        TypeFilter(TypeFilter),
        FirstComponent(String),
        FirstElement(String),
        Error,
    }
    let type_search_ = || (
        optional(attempt((type_filter(), one_of("\t :".chars()), optional(many1::<Vec<_>, _, _>(one_of("\t ".chars()))), optional(one_of("\t :".chars()))).map(|(ty, c1, sp, c2)| {
            match (c1, c2) {
                (':', None) | (':', Some(' ')) | (':', Some('\t')) | (' ', Some(':')) | ('\t', Some(':')) =>
                    TypeFilterOrFirstComponentOrFirstElementOrError::TypeFilter(ty),
                (':', Some(':')) if sp.is_none() =>
                    TypeFilterOrFirstComponentOrFirstElementOrError::FirstComponent(ty.to_string()),
                (' ', Some(' ')) | ('\t', Some('\t')) | (' ', Some('\t')) | ('\t', Some(' ')) | (' ', None) | ('\t', None) =>
                    TypeFilterOrFirstComponentOrFirstElementOrError::FirstElement(ty.to_string()),
                _ =>
                    TypeFilterOrFirstComponentOrFirstElementOrError::Error
            }
        }))),
        arg_list(),
        optional(attempt(return_args()).map(|returned| returned)),
    );
    let type_search = || {
        attempt(type_search_()).then(|(tfofcor, mut arg_list, returned)| {
            let ty = match tfofcor {
                Some(TypeFilterOrFirstComponentOrFirstElementOrError::TypeFilter(ty)) => {
                    Some(ty)
                }
                Some(TypeFilterOrFirstComponentOrFirstElementOrError::FirstComponent(first)) if !arg_list.is_empty() => {
                    arg_list[0].0.insert(0, first);
                    None
                }
                Some(TypeFilterOrFirstComponentOrFirstElementOrError::FirstElement(first)) => {
                    arg_list.insert(0, (vec![first], None));
                    None
                }
                Some(_) => {
                    return unexpected_any("ty::").right();
                }
                None => None
            };
            value((ty, arg_list, returned)).left()
        })
    };

    let query = || {
        choice!(
        attempt(exact_search().map(|(ty, returned, ident, generics): (Option<TypeFilter>, Option<()>, String, Option<Vec<Vec<String>>>)| {
            let ident = normalize_ident(ident);
            let elems = vec![Elem {
                name: ident.clone(),
                full_path: vec![ident.clone()],
                path_without_last: vec![],
                path_last: ident,
                generics: generics.unwrap_or(vec![]).into_iter().map(|path| {
                    let path = path.into_iter().map(normalize_ident).collect::<Vec<String>>();
                    Elem {
                        name: path.join("::"),
                        full_path: path.clone(),
                        path_without_last: Vec::from(&path[..path.len()-1]),
                        path_last: path.last().expect("<path> is parsed with `sep_by1`, and therefore is not empty").to_owned(),
                        generics: vec![],
                    }
                }).collect(),
            }];
            let mut q = Query {
                elems: vec![],
                found_elems: 1,
                original: String::new(), // filled in later
                returned: vec![],
                type_filter: ty.unwrap_or(TypeFilter::Null),
                user_query: String::new(), // filled in later
                error: None,
            };
            if returned.is_some() {
                q.returned = elems;
            } else {
                q.elems = elems;
            }
            q
        })),
        attempt(type_search().map(|(ty, elems, returned): (Option<TypeFilter>, Vec<(Vec<String>, Option<Vec<Vec<String>>>)>, Option<Vec<(Vec<String>, Option<Vec<Vec<String>>>)>>)| {
            let returned = if let Some(returned) = returned {
                returned.into_iter().map(|(path, generics)| {
                    let path = path.into_iter().map(normalize_ident).collect::<Vec<String>>();
                    Elem {
                        name: path.join("::"),
                        full_path: path.clone(),
                        path_without_last: Vec::from(&path[..path.len()-1]),
                        path_last: path.last().expect("<path> is parsed with `sep_by1`, and therefore is not empty").to_owned(),
                        generics: generics.unwrap_or(vec![]).into_iter().map(|path| {
                            let path = path.into_iter().map(normalize_ident).collect::<Vec<String>>();
                            Elem {
                                name: path.last().expect("<path> is parsed with `sep_by1`, and therefore is not empty").to_owned(),
                                full_path: path.clone(),
                                path_without_last: Vec::from(&path[..path.len()-1]),
                                path_last: path.last().expect("<path> is parsed with `sep_by1`, and therefore is not empty").to_owned(),
                                generics: vec![],
                            }
                        }).collect(),
                    }
                }).collect()
            } else {
                vec![]
            };
            let elems: Vec<Elem> = elems.into_iter().map(|(path, generics)| {
                    let path = path.into_iter().map(normalize_ident).collect::<Vec<String>>();
                    Elem {
                        name: path.join("::"),
                        full_path: path.clone(),
                        path_without_last: Vec::from(&path[..path.len()-1]),
                        path_last: path.last().expect("<path> is parsed with `sep_by1`, and therefore is not empty").to_owned(),
                        generics: generics.unwrap_or(vec![]).into_iter().map(|path| {
                            let path = path.into_iter().map(normalize_ident).collect::<Vec<String>>();
                            Elem {
                                name: path.last().expect("<path> is parsed with `sep_by1`, and therefore is not empty").to_owned(),
                                full_path: path.clone(),
                                path_without_last: Vec::from(&path[..path.len()-1]),
                                path_last: path.last().expect("<path> is parsed with `sep_by1`, and therefore is not empty").to_owned(),
                                generics: vec![],
                            }
                        }).collect(),
                    }
                }).collect();
            Query {
                found_elems: elems.len() + returned.len(),
                elems,
                original: String::new(), // filled in later
                returned,
                type_filter: ty.unwrap_or(TypeFilter::Null),
                user_query: String::new(), // filled in later
                error: None,
            }
        }))
    )
    };

    query()
}

fn normalize_ident(s: String) -> String {
    s.to_ascii_lowercase()
}

#[derive(Clone, Deserialize_repr, Eq, PartialEq, Serialize_repr)]
#[repr(i32)]
enum TypeFilter {
    Null = -1,
    Mod = 0,
    ExternCrate = 1,
    Import = 2,
    Struct = 3,
    Enum = 4,
    Fn = 5,
    Type = 6,
    Static = 7,
    Trait = 8,
    Impl = 9,
    TyMethod = 10,
    Method = 11,
    StructField = 12,
    Variant = 13,
    Macro = 14,
    Primitive = 15,
    AssociatedType = 16,
    Constant = 17,
    AssociatedConstant = 18,
    Union = 19,
    ForeignType = 20,
    Keyword = 21,
    Existential = 22,
    Attr = 23,
    Derive = 24,
    TraitAlias = 25,
}

impl FromStr for TypeFilter {
    type Err = String;
    fn from_str(name: &str) -> Result<TypeFilter, String> {
        match name {
            "mod" => Ok(TypeFilter::Mod),
            "externcrate" => Ok(TypeFilter::ExternCrate),
            "import" => Ok(TypeFilter::Import),
            "struct" => Ok(TypeFilter::Struct),
            "enum" => Ok(TypeFilter::Enum),
            "fn" => Ok(TypeFilter::Fn),
            "type" => Ok(TypeFilter::Type),
            "static" => Ok(TypeFilter::Static),
            "trait" => Ok(TypeFilter::Trait),
            "impl" => Ok(TypeFilter::Impl),
            "tymethod" => Ok(TypeFilter::TyMethod),
            "method" => Ok(TypeFilter::Method),
            "structfield" => Ok(TypeFilter::StructField),
            "variant" => Ok(TypeFilter::Variant),
            "macro" => Ok(TypeFilter::Macro),
            "primitive" => Ok(TypeFilter::Primitive),
            "associatedtype" => Ok(TypeFilter::AssociatedType),
            "constant" => Ok(TypeFilter::Constant),
            "associatedconstant" => Ok(TypeFilter::AssociatedConstant),
            "union" => Ok(TypeFilter::Union),
            "foreigntype" => Ok(TypeFilter::ForeignType),
            "keyword" => Ok(TypeFilter::Keyword),
            "existential" => Ok(TypeFilter::Existential),
            "attr" => Ok(TypeFilter::Attr),
            "derive" => Ok(TypeFilter::Derive),
            "traitalias" => Ok(TypeFilter::TraitAlias),
            _ => Err(format!("invalid type filter {name}")),
        }
    }
}

impl ToString for TypeFilter {
    fn to_string(&self) -> String {
        (match *self {
            TypeFilter::Mod => "mod",
            TypeFilter::ExternCrate => "externcrate",
            TypeFilter::Import => "import",
            TypeFilter::Struct => "struct",
            TypeFilter::Enum => "enum",
            TypeFilter::Fn => "fn",
            TypeFilter::Type => "type",
            TypeFilter::Static => "static",
            TypeFilter::Trait => "trait",
            TypeFilter::Impl => "impl",
            TypeFilter::TyMethod => "tymethod",
            TypeFilter::Method => "method",
            TypeFilter::StructField => "structfield",
            TypeFilter::Variant => "variant",
            TypeFilter::Macro => "macro",
            TypeFilter::Primitive => "primitive",
            TypeFilter::AssociatedType => "associatedtype",
            TypeFilter::Constant => "constant",
            TypeFilter::AssociatedConstant => "associatedconstant",
            TypeFilter::Union => "union",
            TypeFilter::ForeignType => "foreigntype",
            TypeFilter::Keyword => "keyword",
            TypeFilter::Existential => "existential",
            TypeFilter::Attr => "attr",
            TypeFilter::Derive => "derive",
            TypeFilter::TraitAlias => "traitalias",
            TypeFilter::Null => "<null>",
        }).to_string()
    }
}

#[derive(Deserialize, Eq, PartialEq, Serialize)]
struct Query {
    elems: Vec<Elem>,
    #[serde(rename = "foundElems")]
    found_elems: usize,
    original: String,
    returned: Vec<Elem>,
    #[serde(rename = "typeFilter")]
    type_filter: TypeFilter,
    #[serde(rename = "userQuery")]
    user_query: String,
    error: Option<String>,
}

#[derive(Deserialize, Eq, PartialEq, Serialize)]
struct Elem {
    name: String,
    #[serde(rename = "fullPath")]
    full_path: Vec<String>,
    #[serde(rename = "pathWithoutLast")]
    path_without_last: Vec<String>,
    #[serde(rename = "pathLast")]
    path_last: String,
    generics: Vec<Elem>,
}

const USAGE: &'static str =
    "Usage: rustdoc-search-ref query | --compare a.json b.json | --compare-with query";

fn main() -> Result<(), String> {
    let mut args = std::env::args();
    let _exe = args.next().ok_or_else(|| USAGE.to_owned())?;
    let query_string = args.next().ok_or_else(|| USAGE.to_owned())?;
    if query_string == "--compare" {
        use std::fs::File;
        let af = args.next().ok_or_else(|| USAGE.to_owned())?;
        let bf = args.next().ok_or_else(|| USAGE.to_owned())?;
        let a: Query = serde_json::from_reader(File::open(&af).map_err(|e| e.to_string())?)
            .map_err(|e| e.to_string())?;
        let b: Query = serde_json::from_reader(File::open(&bf).map_err(|e| e.to_string())?)
            .map_err(|e| e.to_string())?;
        compare_mode(&af, &bf, a, b)?;
    } else if query_string == "--compare-with" {
        let af = "rustdoc.json";
        let bf = "rustdoc-search-ref.json";
        let query_string = args.next().ok_or_else(|| USAGE.to_owned())?;
        let rustdoc_search_ref = parse_mode(&query_string);
        let a: Query =
            serde_json::from_reader(std::io::stdin().lock()).map_err(|e| e.to_string())?;
        let b: Query = serde_json::from_str(&rustdoc_search_ref).map_err(|e| e.to_string())?;
        compare_mode(&af, &bf, a, b)?;
    } else {
        let json = parse_mode(&query_string);
        println!("{json}");
    }
    Ok(())
}

fn parse_mode(query_string: &str) -> String {
    let query_string = query_string.trim();
    let (mut query, rem) = query_().easy_parse(query_string).unwrap_or_else(|e| {
        (
            Query {
                elems: vec![],
                found_elems: 0,
                original: String::new(),
                returned: vec![],
                type_filter: TypeFilter::Null,
                user_query: String::new(),
                error: Some(e.to_string()),
            },
            "",
        )
    });
    if rem != "" && query.error.is_none() {
        query.error = Some(format!("unexpected {rem}"));
    }
    query.original = query_string.to_owned();
    query.user_query = query_string.to_ascii_lowercase();
    serde_json::to_string_pretty(&query).expect("collect to string")
}

fn compare_mode(af: &str, bf: &str, mut a: Query, mut b: Query) -> Result<(), String> {
    // Blank out the error strings, and other data, so that we don't have to
    // concern ourselves with trying to ensure this implementation matches
    // search.js exactly.
    if let Some(ref mut err) = a.error {
        *err = String::from("ERROR");
        a.elems = Vec::new();
        a.returned = Vec::new();
        a.found_elems = 0;
        a.type_filter = TypeFilter::Null;
    }
    if let Some(ref mut err) = b.error {
        *err = String::from("ERROR");
        b.elems = Vec::new();
        b.returned = Vec::new();
        b.found_elems = 0;
        b.type_filter = TypeFilter::Null;
    }
    if a == b {
        Ok(())
    } else {
        let ap = serde_json::to_string_pretty(&a).expect("collect to string");
        let bp = serde_json::to_string_pretty(&b).expect("collect to string");
        let diff = unified_diff::diff(ap.as_bytes(), af, bp.as_bytes(), bf, 3);
        let diff = String::from_utf8_lossy(&diff);
        print!("{diff}");
        Err(String::from("FAIL"))
    }
}
